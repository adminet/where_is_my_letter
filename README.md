**Games in javascript for children (3-5 years) who learning english letters.**

In the project I put emphasis on vector graphics. 
SVG was embed in HTML and manipulate using JavaScript. 
I have used with such JS libraries as GSAP GreenSock and KUTE.
I used the voice from the Google translator.

During design, I used the MVC model


*Rules:*

Creatures with capital and small letters appear on the stage. 
After the start they run to randomly hide behind various things.
The player have to find the matching pair of letters.

There is the three levels difficulty. 
The levels have more pairs of letters, and letters is more similar, for example m and n or d and b. 


https://guydiamond.github.io/where_is_my_letter/

![alt text](https://raw.githubusercontent.com/GuyDiamond/where_is_my_letter/master/img/pic-readme.jpg)

*The game works properly with Chrome
